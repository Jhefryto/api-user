import { IsEmail, IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateUserDtos {
  @IsString()
  @IsOptional()
  nombre: string;

  @IsString()
  @IsOptional()
  apellidos: string;

  @IsEmail()
  @IsOptional()
  email: string;

  // @IsString()
  // @IsOptional()
  // usuario: string;

  // @IsString()
  // @IsOptional()
  // clave: string;

  // @IsNumber()
  // @IsOptional()
  // numeroDiasCambiarClave: number;

  @IsNumber()
  @IsOptional()
  codigoAvatar: number;

  @IsNumber()
  @IsOptional()
  codigoRol: number;

  @IsNumber()
  @IsOptional()
  codigoEmpresa: number;

  @IsNumber()
  @IsOptional()
  codigoUsuario: number;
}
