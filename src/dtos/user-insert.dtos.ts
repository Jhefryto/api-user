import { Exclude } from 'class-transformer';
import { IsEmail, IsNotEmpty, IsNumber, IsString } from 'class-validator';

// @Exclude()
export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  readonly nombre: string;

  @IsString()
  @IsNotEmpty()
  readonly apellidos: string;

  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

  @IsString()
  @IsNotEmpty()
  readonly usuario: string;

  @IsString()
  @IsNotEmpty()
  readonly clave: string;

  @IsNumber()
  @IsNotEmpty()
  readonly numeroDiasCambiarClave: number;

  @IsNumber()
  @IsNotEmpty()
  readonly codigoAvatar: number;

  @IsNumber()
  @IsNotEmpty()
  readonly codigoRol: number;

  @IsNumber()
  @IsNotEmpty()
  readonly codigoEmpresa: number;

  @IsNumber()
  @IsNotEmpty()
  readonly codigoUsuario: number;
}
