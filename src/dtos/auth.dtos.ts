// import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

// @Exclude()
export class LoginAuthDto {
  @IsString()
  @IsNotEmpty()
  readonly usuario: string;

  @IsString()
  @IsNotEmpty()
  readonly clave: string;

  @IsString()
  @IsNotEmpty()
  readonly codEmpresa: string;
}
