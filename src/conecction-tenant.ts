import { Connection, createConnection, getConnection } from 'typeorm';
import { User } from './entities/user.entity';

abstract class DataConecction {
  private _name: string;

  constructor(name: string) {
    this._name = name;
  }

  get name(): string {
    return this._name;
  }

  set name(name: string) {
    this._name = name;
  }
}

export class TenantConection extends DataConecction {
  constructor(name: string) {
    super(name);
  }
  async validateConection(): Promise<Connection> {
    try {
      const conection: Connection = getConnection(super.name);
      return conection;
    } catch (err) {
      const createdConnection: Connection = await createConnection({
        name: super.name,
        type: 'postgres',
        host: process.env.POSTGRES_HOST,
        port: parseInt(process.env.POSTGRES_PORT, 10),
        username: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD,
        database: super.name,
        entities: [],
        // ssl: true,
        synchronize: true,
      });
      return createdConnection;
    }

    // return `El nombre dela base datos es=>${super.name}`;
  }
}
