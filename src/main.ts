import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.TCP,
      options: {
        host: '192.168.30.16',
        port: 3002,
      },
    },
  );
  // await app.startAllMicroservices();
  await app.listen();
}
bootstrap();
