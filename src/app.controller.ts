import {
  ClassSerializerInterceptor,
  Controller,
  UseInterceptors,
} from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { AppService } from './app.service';
import { LoginAuthDto } from './dtos/auth.dtos';
import { CreateUserDto } from './dtos/user-insert.dtos';
import { Express } from 'express';
// import { UpdateUserDtos } from './dtos/usuario-update.dtos';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @UseInterceptors()
  @MessagePattern({ cmd: 'get_user' })
  async getUser(dataLogin: LoginAuthDto): Promise<any> {
    const result = await this.appService.findUser(dataLogin);

    if (!result) {
      return {
        error: 'No Autorizado',
        message: `Usuario y/o Contraseña Incorrecto`,
        statusCode: 401,
      };
    }
    return result;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @MessagePattern({ cmd: 'get_All' })
  async getAll(date: any): Promise<any> {
    const result = await this.appService.findAll(
      date.schema,
      date.page,
      date.perPage,
      date.query,
    );
    return result;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @MessagePattern({ cmd: 'get_One' })
  async getOne(data: any): Promise<any> {
    const id = data.id;
    const schema = data.schema;
    const result = await this.appService.findOne(id, schema);
    return result;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @MessagePattern({ cmd: 'create_user' })
  async create(data: any): Promise<any> {
    // console.log(data);
    const schema = data.schema;
    const newUser = data.newUser as CreateUserDto;
    const file = data.file as Express.Multer.File;
    const result = await this.appService.create(schema, newUser, file);
    return result;
    // try {
    //   const result = await this.appService.create(schema, newUser, file);
    //   return result;
    // } catch (error) {
    //   return {
    //     error: 'Error en la consulta',
    //     message: `El microservicio de subir archivos esta caido`,
    //     statusCode: 503,
    //   };
    // }
  }

  // @UseInterceptors(ClassSerializerInterceptor)
  // @MessagePattern({ cmd: 'update_user' })
  // async update(dataRequest: any): Promise<any> {
  //   const result = await this.appService.update(
  //     dataRequest.schema as string,
  //     dataRequest.id as number,
  //     dataRequest.newUser as UpdateUserDtos,
  //     dataRequest.file as Express.Multer.File,
  //   );
  //   return result;
  // }

  // @UseInterceptors(ClassSerializerInterceptor)
  // @MessagePattern({ cmd: 'delete_user' })
  // async delete(dataRequest: any): Promise<any> {
  //   const id = dataRequest.id as number;
  //   const schema = dataRequest.schema as string;
  //   const result = await this.appService.delete(id, schema);
  //   return result;
  // }
}
