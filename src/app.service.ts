import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Like } from 'typeorm';
import * as bcryptjs from 'bcryptjs';

import { TenantConection } from './conecction-tenant';
import { CreateUserDto } from './dtos/user-insert.dtos';
import { LoginAuthDto } from './dtos/auth.dtos';
import { User } from './entities/user.entity';
import { Client } from 'pg';

@Injectable()
export class AppService {
  private _sp_get_users =
    'SELECT * FROM dbgeneral."fn_gen_get_rol"($1,$2,$3,$4,$5)';
  private _sp_curd_users =
    'CALL dbgeneral."sp_gen_crud_usuario"($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)';
  private _sp_get_login = 'SELECT * FROM dbgeneral."fn_gen_get_login"($1,$2);';
  constructor(
    @Inject('MICRO_DIGITAL') private readonly fileMicroServices: ClientProxy,
    @Inject('PG') private dbGeneral: Client,
  ) {}

  async findUser(dataLogin: LoginAuthDto) {
    const users = await this.dbGeneral.query(this._sp_get_login, [
      dataLogin.codEmpresa,
      dataLogin.usuario,
    ]);
    // console.log(users);
    // const username: string = dataLogin.usuario;
    // const conecction = await new TenantConection(schema).validateConection();
    // const repository = await conecction.getRepository(User);
    // const user = await repository.findOne({ username });
    return users.rows[0];
  }

  async findOne(id: number, schema: string) {
    const users = await this.dbGeneral.query(
      'SELECT * FROM dbgeneral.tbl_gen_usuario WHERE usu_int_codigo=$1 ORDER BY usu_int_codigo ASC ',
      [id],
    );
    // console.log('🚀', users.rows);
    // const conecction = await new TenantConection(schema).validateConection();
    // const repository = await conecction.getRepository(User);
    // const user = await repository.findOne(id);
    return users.rows;
  }

  async findAll(schema: string, page: number, perPage: number, query: string) {
    const users = await this.dbGeneral.query(
      'SELECT * FROM dbgeneral.tbl_gen_usuario ORDER BY usu_int_codigo ASC ',
      [],
    );
    // console.log(users);

    // const conecction = await new TenantConection(schema).validateConection();
    // const repository = await conecction.getRepository(User);
    // const [result, total] = await dbGeneral.findAndCount({
    //   where: { name: Like('%' + query + '%') },
    //   order: { id: 'DESC' },
    //   take: perPage,
    //   skip: perPage * (page - 1),
    // });
    return { data: users.rows, total: users.rowCount };
  }

  async create(schema: string, data: CreateUserDto, file: Express.Multer.File) {
    const hashPassword = await bcryptjs.hash(data.clave, 10);
    const users = await this.dbGeneral.query(
      this._sp_curd_users,
      [
        'I',
        0,
        data.nombre,
        data.apellidos,
        data.email,
        data.usuario,
        hashPassword,
        data.numeroDiasCambiarClave,
        data.codigoAvatar,
        data.codigoRol,
        data.codigoEmpresa,
        data.codigoUsuario,
      ],
      (err, res) => {
        if (err) {
          console.log(err.stack);
          return err.stack;
        } else {
          // console.log(res.rows[0]);
          return res.rows[0];
        }
      },
    );
    // if (users.rows[0].pInt_codigo) {
    //   return {
    //     success: 'Usuario Creado con exito',
    //     message: `Usuario Creado con exito`,
    //     statusCode: 202,
    //   };
    // }
  }

  // async update(
  //   schema: string,
  //   id: number,
  //   data: CreateUserDto,
  //   file: Express.Multer.File,
  // ) {
  //   const userFind = await this.findOne(id, schema);
  //   if (!userFind) {
  //     return {
  //       error: 'Error en la consulta',
  //       message: `El usuario ${data.username} no existe, actulice la lista o consulte su proveedor`,
  //       statusCode: 400,
  //     };
  //   }
  //   data.image = '-';
  //   if (file) {
  //     const imgFTP = await this.fileMicroServices
  //       .send<any>({ cmd: 'upload_imgUser' }, file)
  //       .toPromise();
  //     data.image = imgFTP;
  //   }

  //   const conecction = await new TenantConection(schema).validateConection();
  //   const repository = await conecction.getRepository(User);
  //   const updateUser = repository.merge(userFind, data);
  //   if (data.password) {
  //     const hashPassword = await bcryptjs.hash(updateUser.password, 10);
  //     updateUser.password = hashPassword;
  //   }
  //   return repository.save(userFind);
  // }

  // async delete(id: number, schema: string) {
  //   const userFind = await this.findOne(id, schema);
  //   if (!userFind) {
  //     return {
  //       error: 'Error en la consulta',
  //       message: `El usuario no existe, actulice la lista o consulte su proveedor`,
  //       statusCode: 400,
  //     };
  //   }
  //   const conecction = await new TenantConection(schema).validateConection();
  //   const repository = await conecction.getRepository(User);
  //   const data = {
  //     estado: 0,
  //   };
  //   repository.merge(userFind, data);
  //   return repository.save(userFind);
  // }
}

// const äccesos = [
//   {
//     id: 1,
//     name: 'RRHH',
//     icon: 'URL',
//     descripcion: 'DESCRIPCION',
//     esutlimo: 0, //No es utlimo
//     esModulo: 0, //No es un modulo
//     data: [
//       {
//         id: 1,
//         titulo: 'OPCION 1',
//         ruta: 'opcion1',
//         esutlimo: 1, //ultima opcion
//         default: true,
//       },
//       {
//         id: 2,
//         titulo: 'OPCION 2',
//         esutlimo: 0,
//         data: [{ id: 2, titulo: 'OPCION 2', ruta: 'opcion2', esutlimo: 1 }],
//       },
//     ],
//   },
//   {
//     id: 2,
//     titulo: 'LOGISTICA',
//     esutlimo: 0, //No es ultimo
//     esModulo: 1, //Lo que viene Si es un modulo
//     icon: 'URL',
//     data: [
//       {
//         id: 1,
//         titulo: 'MODULO 1',
//         esutlimo: 0, //No es utlimo
//         esModulo: 1, //Lo que viene Si es un modulo
//         icon: 'URL',
//         descripcion: 'DESCRIPCION',
//         data: [
//           { id: 1, titulo: 'SUB MODULO 1', esutlimo: 1 },
//           { id: 2, titulo: 'SUB MODULO 2', esutlimo: 1 },
//         ],
//       },
//       {
//         id: 2,
//         titulo: 'MODULO 2',
//         esModulo: 0,
//         esutlimo: 0,
//         icon: 'URL',
//         descripcion: 'DESCRIPCION',
//         data: [{ id: 1, titulo: 'OPCION 1', esutlimo: 1, default: true }],
//       },
//     ],
//   },
// ];

// return äccesos
